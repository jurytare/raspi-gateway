Embeded OS for Multi-device connectivity in IoT World

Concept: As a gateway of connectivity of all devices, the Os (Linux platform) will see every device as a device file which has specific driver to handle

Structure:
  - Poky (Yocto project for Linux OS)
  - meta-raspberrypi (for creating OS which is capable in Raspberry pi)
  - meta-osGateway (for creating feature for Linux OS)

Installation: Following steps below or run ./install.sh in ~/. directory
  - Goto poky directory and run source oe-init-build-env rpi_build
  - bitbake core-image-base to build image
The result is the image stored at rpi_build/tmp/deploy/images/raspberrypi/core-image-base.sdrpi_img

